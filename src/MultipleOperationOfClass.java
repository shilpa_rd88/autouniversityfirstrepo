public class MultipleOperationOfClass {

    public static void main(String[] args) {
        /*----- Factorial -------------*/
        // Creating an instance of Factorial class
        Factorial add = new Factorial();
        // Call Factorial function
        int fact_result = add.fact(1,1,5);
        //Print Factorial Result
        System.out.println("Fact of Number is :" + fact_result);

        /*----- Tables -------------*/
        // Creating an instance of Table class
        Tables tbl = new Tables();
        // Call tables function
        tbl.create_table(5);

        /*---------- Reverse Order --------------*/
        int [] arr = {10, 20, 30, 40, 50};
        Reverse rev=new Reverse();
        rev.reverse(arr, arr.length);
    }
}

    class Tables {

        public void create_table(int table) {

            for (int i = 1; i <= 10; i++) {
                System.out.println(table + " * " + i + " = " + table * i);
            }
        }
    }
    class Factorial {

        public int fact(int i, int fact, int num) {

            for (i = 1; i <= num; i++) {
                fact = fact * i;
            }
            return fact;
        }
    }
    class Reverse {
        static void reverse(int a[], int n) {
            int[] b = new int[n];
            int j = n;
            for (int i = 0; i < n; i++) {
                b[j - 1] = a[i];
                j = j - 1;
            }

            /*printing the reversed array*/
            System.out.println("Reversed array is: \n");
            for (int k = 0; k < n; k++) {
                System.out.println(b[k]);
            }
        }
    }

