import java.util.Scanner;

public class Operation {

    public static void main(String args[]) {

        // List of operation
        char c;

        String array[] = {"1. Addition", "2. Subtraction", "3. Multiplication","4. Division","5. MOD"};
        do {
            System.out.println("Operations");
            //Print operation list
            for (String x : array) {
                System.out.println(x);
            }


            Scanner input = new Scanner(System.in);
            // Accept inputs for operation
            System.out.println("================");
            System.out.println("Enter first no:");
            int a = input.nextInt();
            System.out.println("Enter second No:");
            int b = input.nextInt();

            System.out.println("================");

            // Select operation choice
            System.out.println("Enter your choice to continue......");
            int ch = input.nextInt();
            char c1;


            switch (ch) {
                // case statements
                case 1:
                    // Statements
                    System.out.println("Addition is:" + (a + b));
                    break;

                case 2:
                    // Statements
                    System.out.println("Subtraction is:" + (a - b));

                    break;
                case 3:
                    // Statements
                    System.out.println("Multiplication is:" + (a * b));

                    break;
                case 4:
                    // Statements
                    System.out.println("Division is:" + (a / b));

                    break;
                case 5:
                    // Statements
                    System.out.println("Mod:" + (a % b));
                    break;
                default:
                    // Statements
                    System.out.println("Choice is wrong");
            }
            System.out.println("Continue(Y/N)");
            c = input.next().charAt(0);
        } while((c=='Y') || (c=='y'));
        //System.out.println(a + " and " + b);
    }
}
