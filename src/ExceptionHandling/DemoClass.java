package ExceptionHandling;

import java.io.File;
import java.io.IOException;

public class DemoClass {

    public static void main(String[] args) {
      /*  String[] employee = {"Shilpa", "Swapnil", "Akash", "Barkha"};
        try {

            for (int i = 0; i < 5; i++) {
                System.out.println(employee[i]);
            }
        }
            catch(ArrayIndexOutOfBoundsException e){

                System.out.println("Array out of Bound");
                e.printStackTrace();
            }
            */

            createNewFile();
        }

    private static void createNewFile()
    {
        String[] employee = {"Shilpa", "Swapnil", "Akash", "Barkha"};
        File file=new File("resources/tp.txt");
        try{
            for (int i = 0; i < 4; i++) {
                System.out.println(employee[i]);
            }
            file.createNewFile();
        }
        /*catch  (IOException e)
        {
            System.out.println("Directory does not exists");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        catch(ArrayIndexOutOfBoundsException e1){
            System.out.println("Array out of Bound");
            e1.printStackTrace();
    }*/
        catch  (IOException | ArrayIndexOutOfBoundsException e)
        {
            System.out.println("Directory does not exists");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }finally {
            System.out.println("THis will be always executed");
        }
    }


}

