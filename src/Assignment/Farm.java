package Assignment;

public class Farm {

    public static void main(String[] args) {
        Duck d = new Duck();
        d.makeSound();
        d.eat();

        Pig p = new Pig();
        p.makeSound();
        p.eat();
    }
}
