package Assignment;

public abstract class Animal {

    Animal() {
        System.out.println("Animal Constructor");
    }

    public abstract String makeSound();

    public void eat() {
        System.out.println("This is non-abstract method");
    }
}
