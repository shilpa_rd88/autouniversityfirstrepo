package abstraction;

public abstract class Shape
{
    public abstract double calculateArea();
    public void print()
    {
        System.out.println("This is abstract class");
    }
}
