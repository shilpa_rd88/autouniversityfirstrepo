package abstraction;

public class Rectangle extends Shape {

    private double length;
    private double width;

    public Rectangle(double width,double length)
    {
        setLenght(length);
        setWidth(width);
    }

    @Override
    public double calculateArea() {
        return length*width;
    }


    public void setLenght(double lenght) {
        this.length = lenght;
    }



    public void setWidth(double width) {
        this.width = width;
    }
}
