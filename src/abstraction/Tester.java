package abstraction;

abstract public class Tester {

    public static void main(String args[])
    {
        Shape s= new Rectangle(5, 6);
        //Shape w=new Shape();
        s.print();
        System.out.println(s.calculateArea());
    }
}
