package CollectionAssignment;

import java.util.HashMap;
import java.util.Map;

public class StudentResult {


    public static void main(String[] args) {
        // Mapping string values to int keys for map1
        // Mapping string values to int keys for map2
        Map<String, Integer> map1 = test1();
        Map<String, Integer> map2 = test2();


        for (Map.Entry<String, Integer> entry1 : map1.entrySet()) {
            String key = entry1.getKey();
            Integer value1 = entry1.getValue();
            Integer value2 = map2.get(key);
            if(value1>=value2)
                System.out.println(key+"="+value1);
            else
                System.out.println(key+"="+value2);
        }
    }

    private static Map<String, Integer> test1() {
        Map<String, Integer> map1 = new HashMap<String, Integer>();
        map1.put("Shilpa", 60);
        map1.put("Swapnil", 89);
        map1.put("Smita", 94);
        map1.put("Manisha", 50);
        map1.put("Barkha", 89);
        return map1;
    }
    private static Map<String,Integer> test2() {
        // Mapping string values to int keys for map2
        Map<String,Integer> map2 = new HashMap<String,Integer>();
        map2.put("Shilpa", 62);
        map2.put("Swapnil", 80);
        map2.put("Smita", 94);
        map2.put("Manisha", 55);
        map2.put("Barkha", 90);
        return map2;

    }
}
