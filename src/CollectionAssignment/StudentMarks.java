package CollectionAssignment;

import java.util.HashMap;
import java.util.Map;

public class StudentMarks {
        public static void main(String[] args)
        {
            Map<String, Integer> map1 = testOneMarks();
            Map<String, Integer> map2 = testTwoMarks();
            Map Result = new HashMap();

            for (Map.Entry<String, Integer> entry1 : map1.entrySet()) {
                String key = entry1.getKey();
                Integer value1 = entry1.getValue();
                Integer value2 = map2.get(key);
                if(value1>=value2)
                    Result.put(key,value1);
                else
                    Result.put(key,value2);
            }

            System.out.println("================================");
            System.out.println(Result);

            for (Map.Entry<String, Integer> entry1 : map1.entrySet()) {
                String key = entry1.getKey();
                Integer value1 = entry1.getValue();
                Integer value2 = map2.get(key);
                if(value1>=value2)
                    System.out.println(key+"="+value1);
                else
                    System.out.println(key+"="+value2);
            }
        }

    public static Map testOneMarks(){

        Map grades = new HashMap();
        grades.put("Sumeet", 24);
        grades.put("Swapnil", 32);
        grades.put("Akash", 80);
        grades.put("Prince", 50);
        grades.put("Shashi", 79);
        grades.put("Suraj", 40);
        grades.put("Chiranjeev", 59);
        grades.put("Manisha", 55);
        grades.put("Barkha", 95);
        grades.put("Sonali", 63);
        grades.put("Shilpa", 32);

        return grades;
    }

    public static Map testTwoMarks(){

        Map grades = new HashMap();
        grades.put("Sumeet", 97);
        grades.put("Swapnil", 82);
        grades.put("Akash", 76);
        grades.put("Prince", 89);
        grades.put("Shashi", 79);
        grades.put("Suraj", 98);
        grades.put("Chiranjeev", 80);
        grades.put("Manisha", 95);
        grades.put("Barkha", 90);
        grades.put("Sonali", 62);
        grades.put("Shilpa", 79);

        return grades;
    }
    }

