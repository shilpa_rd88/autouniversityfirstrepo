package interfaces;

public class Book implements Product,Car{

    private double Price;
    private String Color;
    private String Name;
    private String Author;
    private int pages;
    private String isbn;
    private String carType;


    @Override
    public double getPrice() {
        return 0;
    }

    @Override
    public String getColor() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setPrice(double price) {

    }

    @Override
    public void setColor(String color) {

    }

    @Override
    public void setName(String name) {

    }

    public String getAuthor() {
        return Author;
    }

    public int getPages() {
        return pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String getCarType() {
        return null;
    }
}
