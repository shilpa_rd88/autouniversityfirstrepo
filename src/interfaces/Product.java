package interfaces;

public interface Product {

    double getPrice();
    String getColor();
    String getName();
    void setPrice(double price);
    void setColor(String color);
    void setName(String name);

    default void getBarcode()
    {
        System.out.println("This is default method");
    }
}
