package Collection;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Map fruit=new HashMap();
        fruit.put("Mango",7);
        fruit.put("Lemon",8);
        fruit.put("Apple",8);
        fruit.put("Mango",2);
        System.out.println(fruit.size());
        System.out.println(fruit);
        System.out.println(fruit.get("Mango"));
        fruit.putIfAbsent("Apple",9);
        fruit.putIfAbsent("Orange",9);

        System.out.println(fruit.entrySet());
        System.out.println(fruit.get("Orange"));
        System.out.println(fruit.keySet());




    }
}
