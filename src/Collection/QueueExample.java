package Collection;

import java.util.LinkedList;
import java.util.Queue;

public class QueueExample {
    public static void main(String[] args) {
        Queue q=new LinkedList();
        q.add("Mango");
        q.add("Orange");
        q.add("Banana");
        q.add("Mango");
        q.add(0);
        q.add(7);
        System.out.println(q.size());
        System.out.println(q);
        System.out.println(q.remove());
        System.out.println(q);
        System.out.println(q.peek());
        System.out.println(q.element());



    }
}
